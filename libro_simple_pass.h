// -*-c++-*-

/*!
  \file libro_simple_pass.h
  \brief simple pass planning & behavior.
*/

/*
 *Copyright:

 Copyright (C) free knowladge funodation

 This code is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.


 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifndef LIBRO_SIMPLE_PASS
#define LIBRO_SIMPLE_PASS

#include <rcsc/player/soccer_action.h>
#include <rcsc/geom/vector_2d.h>


#include <functional>
#include <vector>
#include <boost/concept_check.hpp>


  /*!
  \class Libro_Simple_Pass
  \brief simple pass planning & behavior.
 */
class Libro_Simple_Pass
    : public rcsc::BodyAction {
      
private:
      const rcsc::Vector2D passPoint;
      
public:
      Libro_Simple_Pass(const rcsc::Vector2D & targetPoint):passPoint(targetPoint) 
      { }
      
      /*!
      \brief execute action
      \param agent pointer to the agent itself
      \return true if action is performed
    */
      bool execute( rcsc::PlayerAgent * agent );

  
};

#endif