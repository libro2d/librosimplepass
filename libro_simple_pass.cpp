// -*-c++-*-

/*!
  \file libro_simple_pass.cpp
  \brief simple pass planning & behavior.
*/

/*
 *Copyright:

 Copyright (C) free knowladge foundation

 This code is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.


 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 *EndCopyright:
 */

/////////////////////////////////////////////////////////////////////

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "libro_simple_pass.h"

//#include "intention_kick.h"

//#include <body_kick_one_step.h>
#include<rcsc/action/body_kick_one_step.h>
//#include "body_kick_two_step.h"
//#include "body_kick_multi_step.h"

//#include "body_smart_kick.h"
#include<rcsc/action/body_smart_kick.h>

//#include "body_stop_ball.h"
#include<rcsc/action/body_stop_ball.h>

//#include "body_hold_ball2008.h"
#include<rcsc/action/body_hold_ball2008.h>

#include <rcsc/player/player_agent.h>
#include <rcsc/player/debug_client.h>
#include <rcsc/player/audio_sensor.h>
#include <rcsc/player/say_message_builder.h>

#include <rcsc/common/logger.h>
#include <rcsc/common/server_param.h>
#include <rcsc/geom/rect_2d.h>
#include <rcsc/geom/sector_2d.h>
#include <rcsc/soccer_math.h>
#include <rcsc/math_util.h>
#include<rcsc/common/server_param.h>

using namespace rcsc;
  
  bool Libro_Simple_Pass::execute( PlayerAgent * agent )
  {
    
    const WorldModel & wm = agent->world();

    Vector2D target_point(50.0, 0.0);
    double first_speed = 0.0;
    int receiver = 0;
    
    int kick_step = ( agent->world().gameMode().type() != GameMode::PlayOn
                      && agent->world().gameMode().type() != GameMode::GoalKick_
                      ? 1
                      : 3 );
    
    target_point=passPoint;
    first_speed=rcsc::ServerParam::i().firstBallSpeed(wm.self().pos().dist(passPoint),kick_step);
    
    
    
    Body_SmartKick(target_point,first_speed,first_speed*0.96,kick_step).execute(agent);
     
  }
  

